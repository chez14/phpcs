## CHEZ14's Code Style Standard

The code style standard based on PSR12 with additional extras that chez14 expect
contributors and maintainer to fill in.

Just to make sure everyone write clean documentation for the project they wanted.

Additional Ruleset:
- `PEAR.Commenting.FunctionComment` \
  Make sure to document functions. This will help people that have their IntelliSense enabled on their IDE.

- `PEAR.Commenting.InlineComment` \
  Disable perl style commenting. To chez14, this looks distracting.

### Installation
- Make sure you have installed CodeSniffer: https://github.com/squizlabs/PHP_CodeSniffer

- Add the coding standard to your project.
  ```
  composer req --dev chez14/phpcs squizlabs/php_codesniffer
  ```

- Make phpcs use this standard:
  - By making a new phpcs configuration file:
    - Add `phpcs.xml`
      ```xml
      <?xml version="1.0"?>
      <ruleset name="f3-ilgar">
          <description></description>
          <config name="installed_paths" value="../../chez14/phpcs/src/CHEZ14Standards"/>
          <rule ref="CHEZ14Standards" scope="path"></rule>
      </ruleset>
      ```
      See what filename is acceptable for phpcs: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file \
      See the documentation: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-Ruleset

  - via commandline script
    - Add script to `composer.json`:
      ```json
      {
          "script": {
            "style": "phpcs --standard=./vendor/chez14/phpcs/src/CHEZ14Standards src/"
          }
      }
      ```
      See the documentation: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage#specifying-a-coding-standard

- Add the code sniffer script to the Composer.
  ```json
  {
      "script": {
        "style": "phpcs src/",
        "style:fix": "phpcbf src/"
      }
  }
  ```

### License

[MIT](LICENSE).
